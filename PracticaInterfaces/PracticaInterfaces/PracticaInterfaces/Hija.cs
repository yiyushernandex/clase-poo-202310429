﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaInterfaces
{
    class Hija: Interfaz
    {
        public void Saludo()
        {
            Console.WriteLine("Hola desde clase Hija");
        }
    }
}
