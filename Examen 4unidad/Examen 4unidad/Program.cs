﻿using System;

namespace Examen_4unidad
{
    class Program
    {
        static void Main(string[] args)
        {
            Clase_sobrecarga obj = new Clase_sobrecarga();
            Clase_sobrecarga obj1 = new Clase_sobrecarga();
            abstheredado obj2 = new abstheredado();
            claseInterfaz obj3 = new claseInterfaz();
            Console.WriteLine("SobreCarga");
            obj.Suma(5, 9);
            obj1.Suma(5, 8, 10);
            Console.WriteLine("\nClase Abastacta");
            obj2.Divisionpub(8, 10);
            Console.WriteLine("\nInterfaz");
            obj3.Saludo();
            obj3.Despedida();
        }
    }
}
