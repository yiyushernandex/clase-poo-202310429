﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_4unidad
{
    class Clase_sobrecarga
    {
        public void Suma(int a,int b)
        {
            int resultado = a + b;
            Console.WriteLine("\n{0} + {1} = {2} ", a, b, resultado);

        }
        public void Suma(int a, int b, int c)
        {
            int resultado = a + b + c;
            Console.WriteLine("\n{0} + {1} + {2} = {3} ", a, b, c, resultado);
        }
    }
}
