﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicioclientes
{
    class Program
    {
        static void Main(string[] args)
        {
            string Apellido_Materno = "";
            string Apellido_Paterno = "";
            string CP = "";
            string Dirección = "";
            string ID_Cliente = "";
            string Nombre = "";
            string Telefono = "";
            string Telefono_casa = "";
            string Telefono_movil = "";
            string metodos = "";
            string EliminaCliente = "";

            Console.WriteLine(" Monoblock");
            Console.WriteLine("");
            Console.WriteLine("Clientes");
            Console.WriteLine("");

            Console.Write("Inserta tu Apellido Materno: ");
            Apellido_Materno = Console.ReadLine();
            Console.WriteLine("");

            Console.Write("Inserta tu Apellido Paterno: ");
            Apellido_Paterno = Console.ReadLine();
            Console.WriteLine("");

            Console.Write("Inserta tu Codigo Postal: ");
            CP = Console.ReadLine();
            Console.WriteLine("");

            Console.Write("Inserta tu Dirección: ");
            Dirección = Console.ReadLine();
            Console.WriteLine("");

            Console.Write("Inserta tu ID de Cliente: ");
            ID_Cliente = Console.ReadLine();
            Console.WriteLine("");

            Console.Write("Inserta tu Nombre: ");
            Nombre = Console.ReadLine();
            Console.WriteLine("");

            Console.Write("Inserta tú Telefono: ");
            Telefono = Console.ReadLine();
            Console.WriteLine("");

            Console.Write("Inserta tu Telefono de Casa: ");
            Telefono_casa = Console.ReadLine();
            Console.WriteLine("");

            Console.Write("Inserta tu Telefono Móvil: ");
            Telefono_movil = Console.ReadLine();
            Console.WriteLine("");

            Console.WriteLine("Si desea eliminar el cliente inserte 1, insertar otro 2, mostrar datos 3:");
            metodos = Console.ReadLine();

            Console.WriteLine("");

            if (metodos == "1")
            {
                Console.Clear();
                Apellido_Materno = EliminaCliente;
                Apellido_Paterno = EliminaCliente;
                CP = EliminaCliente;
                Dirección = EliminaCliente;
                ID_Cliente = EliminaCliente;
                Nombre = EliminaCliente;
                Telefono = EliminaCliente;
                Telefono_casa = EliminaCliente;
                Telefono_movil = EliminaCliente;

                Console.WriteLine("Los datos han sido eliminados");
            }

            if (metodos == "2")
            {
                Console.Clear();
                Console.WriteLine("monoblock ");
                Console.WriteLine("");
                Console.WriteLine("Cliente Número 2");
                Console.WriteLine("");

                string Apellido_Materno1 = "";
                string Apellido_Paterno1 = "";
                string CP1 = "";
                string Dirección1 = "";
                string ID_Cliente1 = "";
                string Nombre1 = "";
                string Telefono1 = "";
                string Telefono_casa1 = "";
                string Telefono_movil1 = "";

                Console.Write("Inserta tu Apellido Materno: ");
                Apellido_Materno1 = Console.ReadLine();
                Console.WriteLine("");

                Console.Write("Inserta tu Apellido Paterno: ");
                Apellido_Paterno1 = Console.ReadLine();
                Console.WriteLine("");

                Console.Write("Inserta tu Codigo Postal: ");
                CP1 = Console.ReadLine();
                Console.WriteLine("");

                Console.Write("Inserta tu Dirección: ");
                Dirección1 = Console.ReadLine();
                Console.WriteLine("");

                Console.Write("Inserta tu ID de Cliente: ");
                ID_Cliente1 = Console.ReadLine();
                Console.WriteLine("");

                Console.Write("Inserta tu Nombre: ");
                Nombre1 = Console.ReadLine();
                Console.WriteLine("");

                Console.Write("Inserta tú Telefono: ");
                Telefono1 = Console.ReadLine();
                Console.WriteLine("");

                Console.Write("Inserta tu Telefono de Casa: ");
                Telefono_casa1 = Console.ReadLine();
                Console.WriteLine("");

                Console.Write("Inserta tu Telefono Móvil: ");
                Telefono_movil1 = Console.ReadLine();
                Console.WriteLine("");

                Console.WriteLine("¿Desea Mostrar los Clientes Ingresados? SI o NO (Ingrese la respuesta en mayúscula)");
                metodos = Console.ReadLine();
                Console.Clear();

                if (metodos == "SI")
                {
                    Console.WriteLine("monoblock");
                    Console.WriteLine("");

                    Console.WriteLine("Cliente 1");
                    Console.WriteLine("");

                    Console.WriteLine("Apellido Materno: " + Apellido_Materno);
                    Console.WriteLine("Apellido Paterno: " + Apellido_Paterno);
                    Console.WriteLine("Código Postal: " + CP);
                    Console.WriteLine("Dirección: " + Dirección);
                    Console.WriteLine("Id de Cliente: " + ID_Cliente);
                    Console.WriteLine("Nombre: " + Nombre);
                    Console.WriteLine("Telefono: " + Telefono);
                    Console.WriteLine("Telefono de Casa: " + Telefono_casa1);
                    Console.WriteLine("Telefono Móvil: " + Telefono_movil);
                    Console.WriteLine("");

                    Console.WriteLine("Cliente 2");
                    Console.WriteLine("");

                    Console.WriteLine("Apellido Materno: " + Apellido_Materno1);
                    Console.WriteLine("Apellido Paterno: " + Apellido_Paterno1);
                    Console.WriteLine("Código Postal: " + CP1);
                    Console.WriteLine("Dirección: " + Dirección1);
                    Console.WriteLine("Id de Cliente: " + ID_Cliente1);
                    Console.WriteLine("Nombre: " + Nombre1);
                    Console.WriteLine("Telefono " + Telefono1);
                    Console.WriteLine("Telefono de Casa: " + Telefono_casa);
                    Console.WriteLine("Telefono Móvil: " + Telefono_movil1);
                }

            }
            if (metodos == "3")
            {
                Console.Clear();
                Console.WriteLine("monoblock");
                Console.WriteLine("");

                Console.WriteLine("Cliente 1");
                Console.WriteLine("");

                Console.WriteLine("Apellido Materno: " + Apellido_Materno);
                Console.WriteLine("Apellido Paterno: " + Apellido_Paterno);
                Console.WriteLine("Código Postal: " + CP);
                Console.WriteLine("Dirección: " + Dirección);
                Console.WriteLine("Id de Cliente: " + ID_Cliente);
                Console.WriteLine("Nombre: " + Nombre);
                Console.WriteLine("Telefono" + Telefono);
                Console.WriteLine("Telefono de Casa: " +Telefono_casa);
                Console.WriteLine("Telefono Móvil: " + Telefono);
            }

            if (metodos == "")
            {
                Console.WriteLine("Error");
            }

            Console.ReadKey();
        }
    }
}
