﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rueda
{
    class Program
    {
        static void Main(string[] args)
        {
            int radio_rueda = 10, moneda = 1;

            double area1 = ((3.1416 * radio_rueda) / 2);
            double perimetro1 = 3.1416 * radio_rueda;
            double area2 = ((3.1416 * moneda) / 2);
            double perimetro2 = 3.1416 * moneda;
            Console.WriteLine("El area de la rudeda es: " + area1);
            Console.WriteLine("El perimetro de la rudeda es: " + perimetro1);
            Console.WriteLine("El area de la moneda es: " + area2);
            Console.WriteLine("El perimetro de la moneda es: " + perimetro2);
            Console.ReadLine();
        }
    }
}
