﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace television
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tv TV1 = new tv();
            TV1.settamanio(30);
            int tamanioTV1 = TV1.gettamanio();
            TV1.setvolumen(50);
            int volumenTV1 = TV1.getvolumen();
            TV1.setcontraste(30);
            int contrasteTV1 = TV1.getcontraste();
            TV1.setbrillo(50);
            int brilloTV1 = TV1.getbrillo();
            TV1.setmarca("LG");
            string marcaTV1 = TV1.getmarca();
            TV1.setcolor("Cafe");
            string colorTV1 = TV1.getcolor();
            MessageBox.Show("El tamaño de la TV es " + tamanioTV1 + " Pulgadas\n" + "El Volumen de la TV es " + volumenTV1+"\nEl contraste de la tv es :"+contrasteTV1+"\nEl brillo de la tv es: "+brilloTV1+"\nLa marca de la tv es: "+marcaTV1+"\nEl color de la tv es: "+colorTV1);
        }
    }
}
