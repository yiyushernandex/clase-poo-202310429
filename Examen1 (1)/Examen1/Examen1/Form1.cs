﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Examen1
{
    public partial class Form1 : Form
    {
        auto coche = new auto();
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           
                coche.setpuertas(Convert.ToInt32(comboBox3.SelectedItem));
                int puertas = coche.getpuertas();
                coche.setmarca(comboBox2.SelectedItem.ToString());
                string marca = coche.getmarca();
                coche.setmodelo(comboBox1.SelectedItem.ToString());
                string modelo = coche.getmodelo();
                MessageBox.Show("Su auto seria un " + marca + " " + modelo + " Con " + puertas + " Puertas");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            label4.Visible = true;
                int velocidad = coche.getvelocidad();
            if (velocidad <= 190)
            {
                velocidad = velocidad + 10;
                coche.setvelocidad(velocidad);
                label4.Text = velocidad+"KM/H";
            }
            else
            {
                MessageBox.Show("Ya estas en la maxima velocidad");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            label4.Visible = true;
            int velocidad = coche.getvelocidad();
            if (velocidad >= 10)
            {
                velocidad = velocidad - 10;
                coche.setvelocidad(velocidad);
                label4.Text = velocidad + "KM/H";
            }
            else
            {
                MessageBox.Show("Estas en 0, no puedes desacelerar mas");
            }
        }
    }
}
