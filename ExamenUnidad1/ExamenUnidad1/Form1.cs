﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExamenUnidad1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Celular Cel1 = new Celular();

            Cel1.setTamanio(7);
            int tamamioCel1 = Cel1.getTamanio();
            MessageBox.Show("El tamaño del celular es de :  " + tamamioCel1 + "pulgadas");

            Cel1.setMarca("xiaomi");
            string marcaCel1 = Cel1.getMarca();
            MessageBox.Show("La marca es : " + marcaCel1);


            Cel1.setPeso(115);
            int pesoCel1 = Cel1.getPeso();
            MessageBox.Show("El peso es de: " +pesoCel1 +"gramos");

            Cel1.setRam(3754);
            int ramCel1 = Cel1.getRam();
            MessageBox.Show("La ram del dispositivo es de: " +ramCel1+ "MB");

            Cel1.setColor("Morado");
            string colorCel1 = Cel1.getColor();
            MessageBox.Show("El color del celular es: " +colorCel1);

            Cel1.setAndroid(10);
            int androidCel1 = Cel1.getAndroid();
            MessageBox.Show("El sistema operativo android es: " +androidCel1);

            Cel1.setProcesador("Mediatek Helio G80");
            string procesadorCel1 = Cel1.getProcesador();
            MessageBox.Show("El procesador es: " +procesadorCel1);

            Cel1.setStorage(64);
            int storageCel1 = Cel1.getStorage();
            MessageBox.Show("El espacio del celular es de: " +storageCel1+ "GB");

        }
    }
}
