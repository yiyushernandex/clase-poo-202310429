﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenUnidad1
{
    class Celular
    {
        public int tamanio = 0;
        public string marca = "", color = "";
        public int peso = 0;
        public int ram = 0;
        public string procesador = "";
        public int storage = 0, android = 0;

        public void setTamanio(int tamanio)
        {
            this.tamanio = tamanio;
        }

        public int getTamanio()
        {
            return this.tamanio;
        }

        public void setMarca(string marca)
        {
            this.marca = marca;
        }

        public string getMarca()
        {
            return this.marca;
        }

        public void setPeso(int peso)
        {
            this.peso = peso;
        }

        public int getPeso()
        {
            return this.peso;
        }

        public void setColor(string color)
        {
            this.color = color;
        }

        public string getColor()
        {
            return this.color;
        }

        public void setRam(int ram)
        {
            this.ram = ram;
        }

        public int getRam()
        {
            return this.ram;
        }

        public void setProcesador(string procesador)
        {
            this.procesador = procesador;
        }

        public string getProcesador()
        {
            return this.procesador;
        }

        public void setStorage(int storage)
        {
            this.storage = storage;
        }

        public int getStorage()
        {
            return this.storage;
        }

        public void setAndroid(int android)
        {
            this.android = android;
        }

        public int getAndroid()
        {
            return this.android;
        }


    }
}
