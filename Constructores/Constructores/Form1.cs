﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Constructores
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnCalcularmulta_Click(object sender, EventArgs e)
        {
            int indice = cbxMultas.SelectedIndex;
            Operaciones obj = new Operaciones();
            obj.multa = indice;
            lblResultado.Text = Convert.ToString(obj.CalcularMulta());

        }
    }
}
