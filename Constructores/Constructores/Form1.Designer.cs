﻿namespace Constructores
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbxMultas = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblResultado = new System.Windows.Forms.Label();
            this.btnCalcularmulta = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cbxMultas
            // 
            this.cbxMultas.FormattingEnabled = true;
            this.cbxMultas.Items.AddRange(new object[] {
            "Mal estacionamiento",
            "Exceso de velocidad",
            "Pasarse un alto",
            "Exceso de alcohol"});
            this.cbxMultas.Location = new System.Drawing.Point(354, 65);
            this.cbxMultas.Name = "cbxMultas";
            this.cbxMultas.Size = new System.Drawing.Size(121, 21);
            this.cbxMultas.TabIndex = 0;
            this.cbxMultas.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Baskerville Old Face", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(171, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(133, 14);
            this.label1.TabIndex = 1;
            this.label1.Text = "Seleccione tipo de multa";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(342, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Pago de multa:";
            // 
            // lblResultado
            // 
            this.lblResultado.AutoSize = true;
            this.lblResultado.Location = new System.Drawing.Point(439, 154);
            this.lblResultado.Name = "lblResultado";
            this.lblResultado.Size = new System.Drawing.Size(25, 13);
            this.lblResultado.TabIndex = 3;
            this.lblResultado.Text = "___";
            // 
            // btnCalcularmulta
            // 
            this.btnCalcularmulta.Font = new System.Drawing.Font("Segoe Script", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalcularmulta.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnCalcularmulta.Location = new System.Drawing.Point(326, 114);
            this.btnCalcularmulta.Name = "btnCalcularmulta";
            this.btnCalcularmulta.Size = new System.Drawing.Size(149, 23);
            this.btnCalcularmulta.TabIndex = 4;
            this.btnCalcularmulta.Text = "Calcular Multa";
            this.btnCalcularmulta.UseVisualStyleBackColor = true;
            this.btnCalcularmulta.Click += new System.EventHandler(this.btnCalcularmulta_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 241);
            this.Controls.Add(this.btnCalcularmulta);
            this.Controls.Add(this.lblResultado);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbxMultas);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbxMultas;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblResultado;
        private System.Windows.Forms.Button btnCalcularmulta;
    }
}

