﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsPFP1U4
{
    public class Rectangulo : Figura
    {
        public double Largo;
        public double Ancho;
        public double Total;

        public override void Calcula_Area(double a, double b)
        {
            Total = a * b;
            
        }


    }
}