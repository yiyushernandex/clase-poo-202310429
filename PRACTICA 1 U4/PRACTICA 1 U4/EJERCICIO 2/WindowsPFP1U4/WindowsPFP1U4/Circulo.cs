﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsPFP1U4
{
    public class Circulo : Figura
    {
        public double Radio;

        public override void Calcula_Area(double a, double b)
        {
            Radio = a*a*3.14;
        }
    }
}