﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsPFP1U4
{
    public class Triangulo : Figura
    {
        public double Altura;
        public double Ancho;
        public double Total;

        public override void Calcula_Area(double a, double b)
        {
            Altura = a;
            Ancho= b;

            Total = (Ancho * Altura)/2;
        }
    }
}