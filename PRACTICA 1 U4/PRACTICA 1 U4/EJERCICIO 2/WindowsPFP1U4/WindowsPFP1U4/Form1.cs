﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsPFP1U4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcula_Click(object sender, EventArgs e)
        {
            if(rbRectangulo.Checked==true)
            {
                Rectangulo ObjR = new Rectangulo();
                double anchoR = double.Parse(txtAncho.Text);
                double largoR = double.Parse(txtLargo.Text);
                ObjR.Calcula_Area(anchoR, largoR);
                MessageBox.Show("El area del Rectangulo es: " + ObjR.Total);
            }

           

            if (rbTriangulo.Checked==true)
            {
                Triangulo ObjT = new Triangulo();
                double anchoT = double.Parse(txtAncho.Text);
                double alturaT = double.Parse(txtAltura.Text);
                ObjT.Calcula_Area(anchoT, alturaT);
                MessageBox.Show("El area del Triangulo es: " + ObjT.Total);
               

            }

            if (rbCirculo.Checked==true)
            {
                Circulo ObjC = new Circulo();
                double radioC = double.Parse(txtRadio.Text);
                ObjC.Calcula_Area(radioC, 0);
                MessageBox.Show("El area del circulo es: " + ObjC.Radio);
             


            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void rbRectangulo_CheckedChanged(object sender, EventArgs e)
        {
            if (rbRectangulo.Checked == true)
            {
                txtAltura.Enabled = false;
                txtRadio.Enabled = false;

            }

            else
            {

                txtAltura.Enabled = true;
                txtRadio.Enabled = true;

            }

        }

        private void rbCirculo_CheckedChanged(object sender, EventArgs e)
        {
            if (rbCirculo.Checked == true)
            {
                txtLargo.Enabled = false;
                txtAncho.Enabled = false;
                txtAltura.Enabled = false;

            }

            else
            {
                txtLargo.Enabled = true;
                txtAncho.Enabled = true;
                txtAltura.Enabled = true;
            }


        }

        private void rbTriangulo_CheckedChanged(object sender, EventArgs e)
        {
            if (rbTriangulo.Checked == true)
            {
                txtLargo.Enabled = false;
                txtRadio.Enabled = false;

            }
            else
            {
                txtLargo.Enabled = true;
                txtRadio.Enabled = true;
            }


        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
