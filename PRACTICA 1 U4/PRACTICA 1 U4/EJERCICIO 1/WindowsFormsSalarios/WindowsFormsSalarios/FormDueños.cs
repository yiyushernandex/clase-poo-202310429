﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsSalarios
{
    public partial class FormDueños : Form
    {
        public FormDueños()
        {
            InitializeComponent();
        }

        private void btnCalcula_Click(object sender, EventArgs e)
        {
            int horasT;
            int horasE;
            int valorhoraT = 200;
            int valorhoraE = 400;

            horasT = int.Parse(txtHrsT.Text);
            horasE = int.Parse(txtHrsE.Text);
            Dueños ObjD = new Dueños();
            ObjD.CalculaSalario(horasT, horasE, valorhoraT, valorhoraE);
            label4.Text = "El salario de dueños es de $" + ObjD.pago;
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
