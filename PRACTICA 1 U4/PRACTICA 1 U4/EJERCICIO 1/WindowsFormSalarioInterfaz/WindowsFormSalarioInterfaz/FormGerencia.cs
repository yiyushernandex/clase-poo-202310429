﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormSalarioInterfaz
{
    public partial class FormGerencia : Form
    {
        public FormGerencia()
        {
            InitializeComponent();
        }

        private void btnCalcula_Click(object sender, EventArgs e)
        {
            int horasT;
            int horasE;
            int valorhoraT = 120;
            int valorhoraE = 240;

            horasT = int.Parse(txtHrsT.Text);
            horasE = int.Parse(txtHrsE.Text);
            Gerencia ObjG = new Gerencia();
            ObjG.CalculaSalario(horasT, horasE, valorhoraT, valorhoraE);
            label4.Text = "El salario de gerencia es de $" + ObjG.pago;
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
