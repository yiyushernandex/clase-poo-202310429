﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormSalarioInterfaz
{
    public partial class FormConserje : Form
    {
        public FormConserje()
        {
            InitializeComponent();
        }

        private void btnCalcula_Click(object sender, EventArgs e)
        {
            int horasT;
            int horasE;
            int valorhoraT = 20;
            int valorhoraE = 40;

            horasT = int.Parse(txtHrsT.Text);
            horasE = int.Parse(txtHrsE.Text);
            Conserje ObjC = new Conserje();
            ObjC.CalculaSalario(horasT, horasE, valorhoraT, valorhoraE);
            label4.Text = "El salario de conserje es de $" + ObjC.pago;
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
