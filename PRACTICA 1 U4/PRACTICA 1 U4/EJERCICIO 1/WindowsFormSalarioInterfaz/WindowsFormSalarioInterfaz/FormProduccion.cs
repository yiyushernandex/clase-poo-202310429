﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormSalarioInterfaz
{
    public partial class FormProduccion : Form
    {
        public FormProduccion()
        {
            InitializeComponent();
        }

        private void btnCalcula_Click(object sender, EventArgs e)
        {
            int horasT;
            int horasE;
            int valorhoraT = 35;
            int valorhoraE = 70;

            horasT = int.Parse(txtHrsT.Text);
            horasE = int.Parse(txtHrsE.Text);
            Produccion ObjP = new Produccion();
            ObjP.CalculaSalario(horasT, horasE, valorhoraT, valorhoraE);
            label4.Text = "El salario de produccion es de $" + ObjP.pago;
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
