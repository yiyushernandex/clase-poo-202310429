﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormSalarioInterfaz
{
    public partial class FormAdministrativos : Form
    {
        public FormAdministrativos()
        {
            InitializeComponent();
        }

        private void btnCalcula_Click(object sender, EventArgs e)
        {
            int horasT;
            int horasE;
            int valorhoraT = 50;
            int valorhoraE = 100;

            horasT = int.Parse(txtHrsT.Text);
            horasE = int.Parse(txtHrsE.Text);
            Administrativo ObjA = new Administrativo();
            ObjA.CalculaSalario(horasT, horasE, valorhoraT, valorhoraE);
            label4.Text = "El salario de administrativos es de $" + ObjA.pago;
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
