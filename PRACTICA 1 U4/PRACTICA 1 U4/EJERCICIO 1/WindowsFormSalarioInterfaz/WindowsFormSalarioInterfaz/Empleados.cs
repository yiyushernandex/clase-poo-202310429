﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsFormSalarioInterfaz
{
    public interface Empleados
    {
        void CalculaSalario(int horasT, int horasE, int costohoraT, int costohoraE);
    }
}