﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormSalarioAbstract
{
    public partial class FormAdministrativo : Form
    {
        public FormAdministrativo()
        {
            InitializeComponent();
        }

        private void btnCalcula_Click(object sender, EventArgs e)
        {
            int horasT;
            int horasE;
            int valorhoraT = 50;
            int valorhoraE = 100;

            horasT = int.Parse(txtHrsT.Text);
            horasE = int.Parse(txtHrsE.Text);
            Conserje ObjC = new Conserje();
            ObjC.CalculaSalario(horasT, horasE, valorhoraT, valorhoraE);
            label4.Text = "El salario de administrativo es de $" + ObjC.pago;
        }

        private void btnVolver_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
