﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Examen_tercera_unidad
{
    class Class1
    {
        int suma1;
        public void entrada1()
        {
            suma1 = 8 + 8;
            Console.WriteLine("suma de 8+8 = {0}", suma1);
        }
        private void entrada2()
        {
            suma1 = 8 + 5;
            Console.WriteLine("suma de 8+5 = {0}", suma1);
        }
        protected void entrada3()
        {
            suma1 = 8 + 9;
            Console.WriteLine("suma de 8+9 = {0}", suma1);
        }
        public void entrada2pub()
        {
            entrada2();
        }
    }
    class Clase2:Class1
    {
        public void entra1c2()
        {
            entrada1();
        }
        public void entrada3c2()
        {
            entrada3();
        }
        public void entrada2c2()
        {
            entrada2pub();
        }
    }
    public class Constructor
    {
        private int num1, num2;
        public Constructor(int nume1=7,int nume2=16)
        {
            num1 = nume1;
            num2 = nume2;
            num1 = num1 + num2;
            Console.WriteLine("El resultado de {0} + {1} = {2}", nume1, num2, num1);
        }
        ~Constructor()
        {
            Console.WriteLine("El destructor ha sido llamado");
        }
       
    }
    
}
