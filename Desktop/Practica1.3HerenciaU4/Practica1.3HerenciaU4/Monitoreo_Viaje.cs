﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica1._3HerenciaU4
{
    class Monitoreo_Viaje
    {
        public int nro_monitoreo = 15;
        public string diagnostico_prac = "Bueno";
        public string hora = "12:30";
        public int pulso = 25;
        public int presion = 30;
        public int lat_fetales = 23;
        public int nro_contracciones = 5;
        public int hem_cantidad = 12;
        public int convulciones_hora = 2;
        public int paro_cardio = 1;
        public string otros = "Normal";

    }
    class Accion_realizada: Monitoreo_Viaje
    {
        public int cod_accion = 1432;
        public string descripcion = "Buena";
        public string tipo = "o-";
        private string acceder;

        public void Listar()
        {
            Console.WriteLine("El numero de monitoreo es: "+ nro_monitoreo);
            Console.WriteLine("El diagnostico es: " + diagnostico_prac);
            Console.WriteLine("La hora es: " + hora);
            Console.WriteLine("El puso es de: " + pulso);
            Console.WriteLine("La presion es de: "+ presion);
            Console.WriteLine("Los lat fetales son: " + lat_fetales);
            Console.WriteLine("El numero de contracciones es de: " + nro_contracciones);
            Console.WriteLine("La cantidad de hem es de: " + hem_cantidad);
            Console.WriteLine("Las convulciones por hora son de: " + convulciones_hora);
            Console.WriteLine("El numero de paros cardiacos son de: " + paro_cardio);
            Console.WriteLine("Otras condiciones: " + otros);
            Console.WriteLine("El cod de accion es: " + cod_accion);
            Console.WriteLine("La descripcion es: " + descripcion);
            Console.WriteLine("El tipo es de: " + tipo);
        }

        public void Actualizar()
        {
            Console.WriteLine("¿Quieres actualizar los atributos? si/no = cualquier Tecla");
            acceder = Convert.ToString(Console.ReadLine());
            if (acceder == "si")
            {
                Console.WriteLine("Ingresa el numero de monitoreo");
                nro_monitoreo = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Ingesa el diagostico");
                diagnostico_prac = Convert.ToString(Console.ReadLine());

                Console.WriteLine("Ingresa la hora");
                hora = Convert.ToString(Console.ReadLine());

                Console.WriteLine("Ingresa el pulso");
                pulso = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Ingresa la presion");
                presion = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("ingresa los la fetales");
                lat_fetales = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Ingresa el numero de contracciones");
                nro_contracciones = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Ingresa la cantidades de hem");
                hem_cantidad = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Ingresa las convulciones por hora");
                convulciones_hora = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Ingresa el numero de paros cardiacos");
                paro_cardio = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("otros");
                otros = Convert.ToString(Console.ReadLine());

                Console.WriteLine("Ingresa el codigo de accion");
                cod_accion = Convert.ToInt32(Console.ReadLine());

                Console.WriteLine("Ingresa la descripcion");
                descripcion = Convert.ToString(Console.ReadLine());

                Console.WriteLine("Ingresa el tipo");
                tipo = Convert.ToString(Console.ReadLine());
            }
            else
            {
                Console.WriteLine("Ta bueno");
            }
        }

        public void Guardar()
        {
            Console.WriteLine("Los atributos ingresados son:");
            Console.WriteLine("El numero de monitoreo es: " + nro_monitoreo);
            Console.WriteLine("El diagnostico es: " + diagnostico_prac);
            Console.WriteLine("La hora es: " + hora);
            Console.WriteLine("El puso es de: " + pulso);
            Console.WriteLine("La presion es de: " + presion);
            Console.WriteLine("Los lat fetales son: " + lat_fetales);
            Console.WriteLine("El numero de contracciones es de: " + nro_contracciones);
            Console.WriteLine("La cantidad de hem es de: " + hem_cantidad);
            Console.WriteLine("Las convulciones por hora son de: " + convulciones_hora);
            Console.WriteLine("El numero de paros cardiacos son de: " + paro_cardio);
            Console.WriteLine("Otras condiciones: " + otros);
            Console.WriteLine("El cod de accion es: " + cod_accion);
            Console.WriteLine("La descripcion es: " + descripcion);
            Console.WriteLine("El tipo es de: " + tipo);





        }
    }
}
