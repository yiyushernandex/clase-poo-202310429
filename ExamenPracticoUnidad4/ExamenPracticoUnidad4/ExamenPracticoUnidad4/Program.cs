﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenPracticoUnidad4
{
    class Program
    {
        static void Main(string[] args)
        {

            Operacion ope = new Operacion();
            ope.CalcularOperacion(5, 7);

            SobreCargaMetodos carga = new SobreCargaMetodos();
            carga.Calcular("Este mensaje es de la clase sobrecarga de metodos");
            carga.Calcular(8, 4);

            MensajeInterfaz mensaje = new MensajeInterfaz();
            mensaje.Mostrar();
            mensaje.Crear();

            Console.ReadKey();
        }
    }
}
