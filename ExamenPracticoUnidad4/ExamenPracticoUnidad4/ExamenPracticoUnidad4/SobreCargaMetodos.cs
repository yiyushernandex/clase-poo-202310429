﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenPracticoUnidad4
{
    class SobreCargaMetodos
    {
        public void Calcular(string x)
        {
            Console.WriteLine(x);
        }

        public void Calcular(int x, int y)
        {
            int resultado = x * y;
            Console.WriteLine("El resultado de la multiplicacion en la sobrecarga de metodos es: " + resultado);
        }

       

    }
}
