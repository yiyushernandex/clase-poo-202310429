﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenPracticoUnidad4
{
    class Operacion: MetodosVirtuales1
    {
        public override void CalcularOperacion(double n1, double n2)
        {
            base.CalcularOperacion(n1, n2);
            double resultado = n1 / n2;
            Console.WriteLine("El resultadode la division en la clase operacion es: " + resultado);
        }

        
    }
}
