﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenPracticoUnidad4
{
    class MensajeInterfaz: ClaseInterfaz
    {
        public void Mostrar()
        {
            Console.WriteLine("Mensaje de la interfaz");
        }

        public void Crear()
        {
            Console.WriteLine("segundo mensaje de la interfaz");
        }
    }
}
