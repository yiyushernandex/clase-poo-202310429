﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaHerencia
{
    class Program
    {
        static void Main(string[] args)
        {
            // Se instancia la clase Hijo
            Hijo obj = new Hijo();

            // Se llama al metodo de la clase Herencia por medio de la clase Hijo
            obj.metodo1();
            //Se llama al metodo de la clase Hijo que a su vez el metodo llama al metodo de la clase Herencia
            obj.AccesoMetodo3();

            //Se instancia la clase Herencia
            Herencia obj2 = new Herencia();

            //Se manda a llamar un metodo private de la la clase Herencia
            obj.AccesoMetodo2();


            Console.ReadKey();
        }
    }
}
