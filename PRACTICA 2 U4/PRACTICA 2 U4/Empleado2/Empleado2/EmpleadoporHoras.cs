﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Empleado2
{
    public class EmpleadoporHoras : Empleados
    {
        public override void CalculaSalario(double SA, double SH, int HT)
        {
            if(HT<=40)
            {
                Sueldo = (int)(SH * HT);
            }
            if(HT>40)
            {
                Sueldo= (int)(40 * SH + (HT - 40) * SH * 1.5);
            }
        }
    }
}