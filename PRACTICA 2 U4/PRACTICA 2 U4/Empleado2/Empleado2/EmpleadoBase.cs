﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Empleado2
{
    public class EmpleadoBase : Empleados
    {
        public override void CalculaSalario(double SA, double SH, int HT)
        {
            Sueldo = (int)((SA * SH) + HT);
        }
    }
}