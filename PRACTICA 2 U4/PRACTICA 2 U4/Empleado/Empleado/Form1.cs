﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empleado
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            lsDatos.Items.Clear();
            string Nombre = txtNombre.Text;
            string Apellido = txtApellido.Text;
            string NSS = txtNSS.Text;
            double Salario = double.Parse(txtSalario.Text);
            EmpleadoAsalariado ObjEA = new EmpleadoAsalariado();
            ObjEA.CalculaSalario(Salario, 0, 0);
            lsDatos.Items.Add("Nombre del Empleado: " + Nombre);
            lsDatos.Items.Add("Apellido Empleado: " + Apellido);
            lsDatos.Items.Add("Numero Seguro Social: " + NSS);
            lsDatos.Items.Add("Sueldo Semanal: " + ObjEA.Sueldo);
        }

        private void btnCalcula_Click(object sender, EventArgs e)
        {
            lsDatos.Items.Clear();
            string Nombre = txtNombre.Text;
            string Apellido = txtApellido.Text;
            string NSS = txtNSS.Text;
            double SueldoHora = double.Parse(txtSueldoHora.Text);
            int HorasTrab = int.Parse(txtHorasTrab.Text);
            EmpleadoporHoras ObjEH = new EmpleadoporHoras();
            ObjEH.CalculaSalario(0, SueldoHora, HorasTrab);
            lsDatos.Items.Add("Nombre del Empleado: " + Nombre);
            lsDatos.Items.Add("Apellido Empleado: " + Apellido);
            lsDatos.Items.Add("Numero Seguro Social: " + NSS);
            lsDatos.Items.Add("Sueldo Semanal: " + ObjEH.Sueldo);
        }

        private void btnCalcularEC_Click(object sender, EventArgs e)
        {
            lsDatos.Items.Clear();
            string Nombre = txtNombre.Text;
            string Apellido = txtApellido.Text;
            string NSS = txtNSS.Text;
            int VentasBrutas = int.Parse(txtVentasBrutas.Text);
            double TarifasC = double.Parse(txtTarifasC.Text);
            EmpleadoPorComision ObjEC = new EmpleadoPorComision();
            ObjEC.CalculaSalario(VentasBrutas, TarifasC, 0);
            lsDatos.Items.Add("Nombre del Empleado: " + Nombre);
            lsDatos.Items.Add("Apellido Empleado: " + Apellido);
            lsDatos.Items.Add("Numero Seguro Social: " + NSS);
            lsDatos.Items.Add("Sueldo Semanal: " + ObjEC.Sueldo);
        }

        private void btnCalcularEB_Click(object sender, EventArgs e)
        {
            lsDatos.Items.Clear();
            string Nombre = txtNombre.Text;
            string Apellido = txtApellido.Text;
            string NSS = txtNSS.Text;
            int VentasBrutas = int.Parse(txtVentasBrutas.Text);
            double TarifasC = double.Parse(txtTarifasC.Text);
            int SalarioBase = int.Parse(txtSueldoBase.Text);
            EmpleadoBase ObjEB = new EmpleadoBase();
            ObjEB.CalculaSalario(VentasBrutas, TarifasC, SalarioBase);
            lsDatos.Items.Add("Nombre del Empleado: " + Nombre);
            lsDatos.Items.Add("Apellido Empleado: " + Apellido);
            lsDatos.Items.Add("Numero Seguro Social: " + NSS);
            lsDatos.Items.Add("Sueldo Semanal: " + ObjEB.Sueldo);
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void txtSalario_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
